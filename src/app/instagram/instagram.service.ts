import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import {AddressSettings} from "../address-settings";
import {Observable} from "rxjs/Observable";
/**
 * Created by zajac on 12.02.2018.
 */

@Injectable()
export class InstagramService {
  private readonly imagesURL = AddressSettings.INSTAGRAM_IMAGES_URL;

  constructor(private http: Http) {
  }

  public getUserImages(): Observable<Response> {
    return this.http.get(this.imagesURL);
  }
}
