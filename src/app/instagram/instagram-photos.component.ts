/**
 * Created by zajac on 12.02.2018.
 */
import {Component, OnInit} from "@angular/core";
import {InstagramService} from "./instagram.service";
import {Router} from "@angular/router";

@Component({
  selector: 'instagramPhotos',
  templateUrl: 'instagram-photos.component.html'
})

export class InstagramPhotosComponent implements OnInit {
  constructor(private instagramService: InstagramService, private router: Router) {
  }

  imagesUrls: String[];

  ngOnInit(): void {
    this.getInstagramImages();
  }

  private getInstagramImages() {
    this.instagramService.getUserImages().subscribe(urls => {
      this.imagesUrls = urls.json();
    }, error2 => {
      console.log(error2);
    })
  }

  openSingleImage(url: String) {
    // this.router.navigate(['/link-image', url]);
    this.router.navigate(['/link-image', url]);
    // , { queryParams:  filter, skipLocationChange: true}
  }
}
