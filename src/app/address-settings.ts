/**
 * Created by zajac on 24.06.2017.
 */
export class AddressSettings {
  public static DOMAIN_URL = 'http://localhost:8899';
  public static BACKEND_REST_ENDPOINT = '';
  public static INSTAGRAM_IMAGES_URL = AddressSettings.BACKEND_REST_ENDPOINT + '/oauth/user/images-links';
  public static UPLOAD_FILE_URL = AddressSettings.BACKEND_REST_ENDPOINT + '/dispatcher/uploadMultipartFile';
  public static UPLOAD_URL_URL = AddressSettings.BACKEND_REST_ENDPOINT + '/dispatcher/uploadFromLink';
  public static IMAGE_URL = AddressSettings.BACKEND_REST_ENDPOINT + '/img/';
  public static IMAGE_DETAIL_URL = AddressSettings.BACKEND_REST_ENDPOINT + '/image/';
  public static BINARIZED_IMAGE_URL = AddressSettings.BACKEND_REST_ENDPOINT + '/binarized-img/';
  public static COLOR_BINARIZED_IMAGE_URL = AddressSettings.BACKEND_REST_ENDPOINT + '/color-binarized-img/';
  public static LAST_12_IDS_IMAGES_URL = AddressSettings.BACKEND_REST_ENDPOINT + '/img/lasts12';
  public static MY_IMAGES_IDS = AddressSettings.BACKEND_REST_ENDPOINT + '/img/by-user/';
  public static IMAGES_FILTERS_ADDRESSES_URL = AddressSettings.BACKEND_REST_ENDPOINT + '/filters-endpoints';
}
