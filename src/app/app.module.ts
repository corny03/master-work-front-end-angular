import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";
import {MasonryModule} from "angular2-masonry";

import {AppComponent} from "./app.component";
import {AppRoutingModule} from "./app-routing.module";
import {SimpleTestComponent} from "./tester/simple-test.component";
import {TestService} from "./tester/test.service";
import {WelcomeComponent} from "./static/welcome.component";
import {UploaderComponent} from "./uploader/uploader.component";
import {ImageService} from "./uploader/image.service";
import {ImageComponent} from "./uploader/image.component";
import {UserComponent} from "./user/users.component";
import {InstagramPhotosComponent} from "./instagram/instagram-photos.component";
import {UsersService} from "./user/users.service";
import {InstagramService} from "./instagram/instagram.service";
import {ImageFromLinkUploaderComponent} from "./uploader/image-from-link-uploader.component";
import {UploadedByUserComponent} from "./user/uploaded-by-user.component";
import {NavigationComponent} from "./navigation.component";

@NgModule({
  declarations: [
    AppComponent,
    SimpleTestComponent,
    WelcomeComponent,
    UploaderComponent,
    ImageComponent,
    UserComponent,
    InstagramPhotosComponent,
    ImageFromLinkUploaderComponent,
    UploadedByUserComponent,
    NavigationComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    MasonryModule
  ],
  providers: [TestService, ImageService, UsersService, InstagramService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
