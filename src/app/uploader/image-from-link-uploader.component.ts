/**
 * Created by zajac on 12.02.2018.
 */
import {Component, OnInit} from "@angular/core";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {UsersService} from "../user/users.service";
import {Response} from "@angular/http";
import {Principal} from "../user/Principal";
import {ImageService} from "./image.service";

@Component({
  selector: 'imageFromLinkUploader',
  templateUrl: 'image-from-link-uploader.component.html',
  styleUrls: ['image-from-link-uploader.component.css']
})

export class ImageFromLinkUploaderComponent implements OnInit {
  constructor(private router: Router, private route: ActivatedRoute, private userService: UsersService, private imageService: ImageService) {
  }

  idImage: number;
  successMsg = null;
  errorMsg = null;
  error = null;
  sendAsPublic: boolean = false;
  principal: Principal = new Principal(false, 'undefined', 'undefined', 'undefined');
  imgUrl: String = "";

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) =>
      this.imgUrl = (params['imgUrl'])
    );
    this.getUser();
  }


  userPostImage(): void {
    let userName = this.principal.username;
    if (!this.principal.authenticated) {
      this.sendAsPublic = true;
    }
    this.imageService.postFormURL(this.imgUrl, userName, (!this.sendAsPublic)).subscribe(
      response => {
        this.successMsg = 'Wysłano z powodzeniem!!';
        this.idImage = response;
        this.router.navigate(['/image', response]);
      }),
      error =>
        this.errorMsg = 'Niepowodzenie wysłania pliku' ,
      () =>
        this.successMsg = 'Wysłano z powodzeniem!!';
  }

  getUser(): void {
    this.userService.getUser().subscribe((response: Response) => {
      if (response.status <= 300) {
        this.principal = response.json();
      }
    }, (error) => {
      console.log(error);
    });
  }

}
