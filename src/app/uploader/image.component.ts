/**
 * Created by zajac on 09.11.2017.
 */
import {Component, OnInit} from "@angular/core";
import {ActivatedRoute, Params} from "@angular/router";
import {AddressSettings} from "../address-settings";
import {ImageService} from "./image.service";
import {MyAlgorithms} from "../my-algorithms";
import {FileUrlAndStatus} from "./FileUrlAndStatus";
@Component({
  selector: 'image',
  templateUrl: 'image.component.html',
  styleUrls: ['image.component.css']
})

export class ImageComponent implements OnInit {
  imageId: string;
  imgUrl: string;
  imgMainView: string;
  readonly domainURL = AddressSettings.DOMAIN_URL;
  okStatus: string = "OK";
  foundStatus: string = "FOUND";
  notFoundStatus: string = "NOT FOUND";
  minimumRefreshTime: number = 6;
  imgStatus: string = this.notFoundStatus;

  filtersUrls: string[];
  filtersUrlsAndStatuses: FileUrlAndStatus[];
  filtersUrlsBy4: FileUrlAndStatus[][];

  constructor(private route: ActivatedRoute, private imageService: ImageService) {
  }

  ngOnInit(): void {
    this.getImagesFiltersAndSetUrls();
  }

  getImagesFiltersAndSetUrls() {
    this.imageService.getAddressesOfImagesFilters().subscribe(urls => {
      this.filtersUrls = urls;
      console.log(urls);
      this.route.params.subscribe((params: Params) =>
        this.setUrls(params['id'])
      );
      this.autoRefreshProcessingImages();
    });
  }

  autoRefreshProcessingImages() {
    this.minimumRefreshTime--;
    this.checkStatuses();
    let condition = false;
    for (let urlAndStatus of this.filtersUrlsAndStatuses) {
      if (urlAndStatus.status === this.foundStatus || this.minimumRefreshTime > 0) {
        condition = true;
      }
    }
    if (condition === true)
      setTimeout(() => this.autoRefreshProcessingImages(), 250)
  }

  // autoRefreshProcessingImages() {
  //   this.checkStatuses();
  //   if (this.binarizedImgStatus === this.foundStatus ||
  //     this.colorBinarizedImgStatus === this.foundStatus ||
  //     this.firstCheck) {
  //     setTimeout(() => this.autoRefreshProcessingImages(), 1000)
  //   }
  // }

  checkStatuses() {
    this.imageService.checkImage(this.imgUrl).subscribe(resultCode => {
        this.imgStatus = this.translateStatus(resultCode);

        for (let filterUrlAndStatus of this.filtersUrlsAndStatuses) {
          this.imageService.checkImage(filterUrlAndStatus.filterUrl).subscribe(resultCode => {
            filterUrlAndStatus.status = this.translateStatus(resultCode);
          }, err => filterUrlAndStatus.status = this.translateStatus(err.status));
        }

      }, err => {
        this.imgStatus = this.translateStatus(err);
      },
    )
  }

  translateStatus(codeNumber: number): string {
    if (codeNumber == 200) {
      return this.okStatus;
    } else if (codeNumber == 302) {
      return this.foundStatus
    }
    else
      return this.notFoundStatus
  }

  setUrls(id: string): void {
    this.imageId = String(id);
    this.imgMainView = this.imgUrl = AddressSettings.IMAGE_URL + this.imageId;
    this.filtersUrlsAndStatuses = [];
    this.filtersUrlsAndStatuses.push(new FileUrlAndStatus(this.imgUrl, this.imgStatus));
    for (let filterUrl of this.filtersUrls) {
      let addres = AddressSettings.BACKEND_REST_ENDPOINT + filterUrl + this.imageId;
      this.filtersUrlsAndStatuses.push(new FileUrlAndStatus(addres, this.notFoundStatus));
    }
    this.filtersUrlsBy4 = MyAlgorithms.by4Elements(this.filtersUrlsAndStatuses);
  }

  swapMainImg(url: string) {
    this.imgMainView = url;
  }

  //
  // swapMainToMini1(): void {
  //   let temp = this.imgMainView;
  //   this.imgMainView = this.imgMiniView1;
  //   this.imgMiniView1 = temp;
  // }
  //
  // swapMainToMini2(): void {
  //   let temp = this.imgMainView;
  //   this.imgMainView = this.imgMiniView2;
  //   this.imgMiniView2 = temp;
  // }
}
