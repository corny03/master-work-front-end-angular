/**
 * Created by zajac on 22.03.2018.
 */
export class FileUrlAndStatus {
  constructor(filterUrl: string, status: string) {
    this.filterUrl = filterUrl;
    this.status = status;
  }

  public filterUrl: string;
  public status: string;
}
