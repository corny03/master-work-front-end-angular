/**
 * Created by zajac on 07.11.2017.
 */
import {Component, OnInit} from "@angular/core";
import {ImageService} from "./image.service";
import {Router} from "@angular/router";
import {AddressSettings} from "../address-settings";
import {UsersService} from "../user/users.service";
import {Principal} from "../user/Principal";
import {Response} from "@angular/http";

@Component({
  selector: 'uploader',
  templateUrl: './uploader.component.html',
  styleUrls: ['./uploader.component.css']
})
export class UploaderComponent implements OnInit {
  sendAsPublic: boolean = false;
  principal: Principal = new Principal(false, 'undefined', 'undefined', 'undefined');
  file = null;
  successMsg = null;
  errorMsg = null;
  error = null;
  idImage: number;
  lastImagesIds: string[];
  imageToUploadUrl = "";
  imagesUrl = AddressSettings.IMAGE_URL;
  imageDetailsUrl = AddressSettings.IMAGE_DETAIL_URL;

  constructor(private router: Router,
              private imageService: ImageService,
              private userService: UsersService) {
  }

  ngOnInit(): void {
    this.getLastImages();
    this.getUser();
  }

  getLastImages(): void {
    this.imageService.getLastImages().subscribe(ids => {
      this.lastImagesIds = ids;
    });
  }

  getFiles(files: any) {
    let empDataFiles: FileList = files.files;
    this.file = empDataFiles[0];
  }

  postImageFromUrl(): void {
    let userName = this.principal.username;
    if (!this.principal.authenticated) {
      this.sendAsPublic = true;
    }

    this.imageService.postFormURL(this.imageToUploadUrl, userName, (!this.sendAsPublic)).subscribe(
      response => {
        this.successMsg = 'Wysłano z powodzeniem!!';
        this.idImage = response;
        this.router.navigate(['/image', response]);
      }),
      error =>
        this.errorMsg = 'Niepowodzenie wysłania pliku' ,
      () =>
        this.successMsg = 'Wysłano z powodzeniem!!';
  }

  postFile() {
    let userName = this.principal.username;
    if (!this.principal.authenticated)
      this.sendAsPublic = true;
    if (this.file != undefined) {
      this.imageService.postFormData(this.file, userName, !this.sendAsPublic).subscribe(
        response => {
          this.successMsg = 'Wysłano z powodzeniem!!';
          this.idImage = response;
          this.router.navigate(['/image', response]);
        }),
        error =>
          this.errorMsg = 'Niepowodzenie wysłania pliku' ,
        () =>
          this.successMsg = 'Wysłano z powodzeniem!!';
    } else if (this.imageToUploadUrl !== "") {
      this.postImageFromUrl();
    }
    else {
      this.errorMsg = 'Niepowodzenie wysłania pliku';
    }
  }


  getUser(): void {
    this.userService.getUser().subscribe((response: Response) => {
      if (response.status <= 300) {
        this.principal = response.json();
      }
    }, (error) => {
      console.log(error);
    });
  }
}
