import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {Http} from "@angular/http";


import "rxjs/add/operator/toPromise";
import {AddressSettings} from "../address-settings";
/**
 * Created by zajac on 08.11.2017.
 */

@Injectable()
export class ImageService {
  uploadImageFileUrl = AddressSettings.UPLOAD_FILE_URL;
  uploadImageUrlUrl = AddressSettings.UPLOAD_URL_URL;
  myImagesUrl = AddressSettings.MY_IMAGES_IDS;
  lastImagesUrl = AddressSettings.LAST_12_IDS_IMAGES_URL;
  imageFiltersAddresses = AddressSettings.IMAGES_FILTERS_ADDRESSES_URL;

  constructor(private http: Http) {
  }

  public getLastImages(): Observable<string[]> {
    return this.http.get(this.lastImagesUrl).map(res => res.json());
  }

  public getAddressesOfImagesFilters(): Observable<string[]> {
    return this.http.get(this.imageFiltersAddresses).map(res => res.json());
  }

  public getMyImages(userName: String): Observable<string[]> {
    return this.http.get(this.myImagesUrl + userName).map(res => res.json());
  }

  public checkImage(address: string): Observable<number> {
    return this.http.get(address).map(this.extractData);
  }

  extractData(res): number {
    return res.status;
  }

  postFormData(file: File, userName: string, isHidden: boolean): Observable<number> {
    return Observable.fromPromise(new Promise((resolve, reject) => {
      let formData: any = new FormData();
      let xhr = new XMLHttpRequest();

      formData.append('userName', userName);
      formData.append('isHidden', String(isHidden));
      formData.append("file", file, file.name);

      xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
            resolve(xhr.response)
          } else {
            reject(xhr.response)
          }
        }
      };
      xhr.open("POST", this.uploadImageFileUrl, true);
      // xhr.setRequestHeader('userName', 'sda');
      xhr.send(formData)
    }));
  }

  postFormURL(fileURL: String, userName: string, isHidden: boolean): Observable<number> {
    return Observable.fromPromise(new Promise((resolve, reject) => {
      let formData: any = new FormData();
      let xhr = new XMLHttpRequest();

      formData.append('userName', userName);
      formData.append('isHidden', String(isHidden));
      formData.append("imageUrl", fileURL);

      xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
            resolve(xhr.response)
          } else {
            reject(xhr.response)
          }
        }
      };
      xhr.open("POST", this.uploadImageUrlUrl, true);
      // xhr.setRequestHeader('userName', 'sda');
      xhr.send(formData)
    }));
  }
}
