/**
 * Created by zajac on 30.10.2017.
 */
import {Component, OnInit} from "@angular/core";
import {TestService} from "./test.service";
import {TestResult} from "./test-result";
import {Observable} from "rxjs/Observable";
@Component({
  selector: 'simpleTester',
  templateUrl: './simple-test.component.html'
})
export class SimpleTestComponent implements OnInit {
  testResults: TestResult[];
  doneStatus : String = "DONE";
  processingStatus : String = "PROCESSING";
  failedStatus : String = "FAILED";
  notStartedStatus : String = "NOTSTARTED";

  constructor(private testService: TestService) {
  }

  getTestResults() {
    this.testService.getTestResults().repeatWhen(() => {
      return Observable.interval(499)
    })
      .subscribe(tS => this.testResults = tS.json(), error2 => Observable.interval(5000));
  }

  ngOnInit(): void {
    this.getTestResults();
  }

  startTest() {
    this.testService.startTester();
  }
}
