/**
 * Created by zajac on 30.10.2017.
 */

import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import {AddressSettings} from "../address-settings";
import "rxjs/add/operator/toPromise";
import {Observable} from "rxjs/Rx";
import "rxjs/add/operator/retry";
import "rxjs/add/operator/repeat";
import "rxjs/add/operator/repeatWhen";
import "rxjs/add/operator/retryWhen";

@Injectable()
export class TestService {
  private getTestResultURL = AddressSettings.BACKEND_REST_ENDPOINT + '/tester/getTestResults';
  private startTestURL = AddressSettings.BACKEND_REST_ENDPOINT + '/tester/start';

  constructor(private http: Http) {
  }

  public getTestResults(): Observable<Response> {
    return this.http.get(this.getTestResultURL).catch((error: any) =>
      Observable.throw(error.json().error || 'Server error')
    );
  }

  public startTester() : void {
    this.http.post(this.startTestURL, null).toPromise();
  }

  private handleError(error: any): Observable<any> {
    Observable.interval(5000);
    return Observable.throw('problem with connection to backend server');
  }
}
