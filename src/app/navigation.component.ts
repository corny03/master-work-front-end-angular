import {Component} from "@angular/core";
import {Principal} from "./user/Principal";
import {UsersService} from "./user/users.service";
import {Router} from "@angular/router";
import {Response} from "@angular/http";
/**
 * Created by zajac on 26.02.2018.
 */
@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',

  styleUrls: ['./navigation.component.css']
})

export class NavigationComponent {
  principal: Principal = new Principal(false, 'undefined', 'undefined', 'undefined');

  constructor(private userService: UsersService,
              protected router: Router) {
  }

  ngOnInit(): void {
    this.getUser();
  }

  getUser(): void {
    this.userService.getUser().subscribe((response: Response) => {
      if (response.status <= 300) {
        this.principal = response.json();
      }
    }, (error) => {
      console.log('Unauthorized');
    });
  }
}
