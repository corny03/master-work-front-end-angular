import {Component} from "@angular/core";
import {UsersService} from "./user/users.service";
import {Principal} from "./user/Principal";
import {Response} from "@angular/http";
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'OneWeekPhoto™';
  principal: Principal = new Principal(false, 'undefined', 'undefined', 'undefined');

  constructor(private userService: UsersService,
              protected router: Router) {
  }

  ngOnInit(): void {
    this.getUser();
  }

  getUser(): void {
    this.userService.getUser().subscribe((response: Response) => {
      if (response.status <= 300) {
        this.principal = response.json();
      }
    }, (error) => {
      console.log('Unauthorized');
    });
  }
}
