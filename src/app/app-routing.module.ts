import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {SimpleTestComponent} from "./tester/simple-test.component";
import {WelcomeComponent} from "./static/welcome.component";
import {UploaderComponent} from "./uploader/uploader.component";
import {ImageComponent} from "./uploader/image.component";
import {UserComponent} from "./user/users.component";
import {InstagramPhotosComponent} from "./instagram/instagram-photos.component";
import {ImageFromLinkUploaderComponent} from "./uploader/image-from-link-uploader.component";
import {UploadedByUserComponent} from "./user/uploaded-by-user.component";
import {NavigationComponent} from "./navigation.component";

const routes: Routes = [
  {path: 'about', component: WelcomeComponent},
  {path: 'main', component: NavigationComponent},
  {path: 'simpleTester', component: SimpleTestComponent},
  {path: 'uploader', component: UploaderComponent},
  {path: 'image/:id', component: ImageComponent},
  {path: 'user', component: UserComponent},
  {path: 'instagram', component: InstagramPhotosComponent},
  {path: 'link-image/:imgUrl', component: ImageFromLinkUploaderComponent},
  {path: 'uploaded-by-user', component: UploadedByUserComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {
}
