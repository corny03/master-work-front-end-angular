/**
 * Created by zajac on 12.02.2018.
 */
export class MyAlgorithms {
  public static by4Elements(someArray: any[]): any[][] {
    let elementsInRow = 4;
    let rows = someArray.length / elementsInRow;
    let rest = someArray.length % elementsInRow;
    rows = rows - (rest / elementsInRow);
    if (rest > 0)
      rows = rows + 1;
    let resultArray = new Array(rows);
    let i = elementsInRow;
    let row = 0;
    for (let idImage of someArray) {

      if (i == 4) {
        resultArray[row] = new Array(4);
        row++;
        i = 0;
      }
      resultArray[row - 1][i] = idImage;
      i++;
    }
    return resultArray;
  }
}
