/**
 * Created by zajac on 07.02.2018.
 */
export class Principal {

  constructor(authenticated: boolean, username: string, full_name: string, profile_picture_url: string) {
    this.username = username;
    this.full_name = full_name;
    this.profile_picture_url = profile_picture_url;
    this.authenticated = authenticated;
  }

  public authenticated: boolean;
  public username: string;
  public full_name: string;
  public profile_picture_url: string;
}
