/**
 * Created by zajac on 15.02.2018.
 */
import {Component} from "@angular/core";
import {AddressSettings} from "../address-settings";
import {Response} from "@angular/http";
import {ImageService} from "../uploader/image.service";
import {UsersService} from "./users.service";
import {Principal} from "./Principal";
@Component({
  selector: 'uploadedByUser',
  templateUrl: 'uploaded-by-user.component.html'
})

export class UploadedByUserComponent {
  constructor(private imageService: ImageService, private userService: UsersService) {
  }

  principal: Principal = new Principal(false, 'undefined', 'undefined', 'undefined');
  file = null;
  error = null;
  // idImagesGrid4x: number[][];
  imagesIds: string[];
  imageUrl = AddressSettings.IMAGE_URL;
  imageDetailsUrl = AddressSettings.IMAGE_DETAIL_URL;


  ngOnInit(): void {
    this.getUserAndImages();
  }

  getMyImages(): void {
    this.imageService.getMyImages(this.principal.username).subscribe(ids => {
      this.imagesIds = ids;
    });
  }

  getUserAndImages(): void {
    this.userService.getUser().subscribe((response: Response) => {
      if (response.status <= 300) {
        this.principal = response.json();
        this.getMyImages();
      }
    }, (error) => {
      console.log(error);
    });
  }
}
