import {Injectable} from "@angular/core";
import {Headers, Http, RequestOptions, Response} from "@angular/http";
import {Cookie} from "ng2-cookies/ng2-cookies";
import {Observable} from "rxjs/Observable";
/**
 * Created by zajac on 29.01.2018.
 */
@Injectable()
export class UsersService {
  private readonly usersURL = '/oauth/user/self';  // URL to web api
  private readonly logoutURL = '/oauth/logout';  // URL to web api

  constructor(private http: Http) {
  }

  getUser(): Observable<Response> {
    return this.http.get(this.usersURL, this.makeOptions())
  }

  logout(): Observable<Response> {
    Cookie.deleteAll();
    return this.http.post(this.logoutURL, '', this.makeOptions())
  }

  // private handleError(error: any): Promise<any> {
  //   return Promise.reject('User not logged in.');
  // }

  private makeOptions(): RequestOptions {
    let headers = new Headers({'Content-Type': 'application/json'});
    //     'X-XSRF-TOKEN': Cookie.get('XSRF-TOKEN')
    return new RequestOptions({headers: headers});
  }
}
