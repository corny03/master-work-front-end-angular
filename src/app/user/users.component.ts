import {Component, OnInit} from "@angular/core";

import {UsersService} from "./users.service";
import {Principal} from "./Principal";
import {Response} from "@angular/http";

@Component({
  selector: 'user',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
  providers: [UsersService]
})

export class UserComponent implements OnInit {
  principal: Principal = new Principal(false, 'undefined', 'undefined', 'undefined');
  // principal: Principal = new Principal(false, 'ADamzajacas', 'Adam undefined', 'https://scontent-frx5-1.cdninstagram.com/vp/3989ec07b89b98ff92579a3c2e022eac/5B2A81B5/t51.2885-19/s150x150/24327328_1526892724058489_7935518903373922304_n.jpg');
  instagramLoginLink: String = "/oauth/login/instagram";

  constructor(private userService: UsersService) {
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.getUser();
    }, 250);
  }

  getUser(): void {
    this.userService.getUser().subscribe((response: Response) => {
      if (response.status <= 300) {
        this.principal = response.json();
      }
    }, (error) => {
      console.log(error);
    });
  }

  logOut(): void {
    console.log('logout');
    this.userService.logout().subscribe(res => console.log(res.json()),
      error2 => console.log(error2.json()));
    window.location.href = "/";
  }
}
